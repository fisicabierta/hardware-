EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L USB_A P1
U 1 1 5817A71A
P 4500 3600
F 0 "P1" H 4700 3400 50  0000 C CNN
F 1 "USB_A" H 4450 3800 50  0000 C CNN
F 2 "Connect:USB_A" V 4450 3500 50  0001 C CNN
F 3 "" V 4450 3500 50  0000 C CNN
	1    4500 3600
	0    -1   -1   0   
$EndComp
$Comp
L USB_B P2
U 1 1 5817A763
P 5300 3600
F 0 "P2" H 5500 3400 50  0000 C CNN
F 1 "USB_B" H 5250 3800 50  0000 C CNN
F 2 "Connect:USB_B" V 5250 3500 50  0001 C CNN
F 3 "" V 5250 3500 50  0000 C CNN
	1    5300 3600
	0    1    -1   0   
$EndComp
Wire Wire Line
	5000 3500 4800 3500
Wire Wire Line
	4800 3600 5000 3600
Wire Wire Line
	5000 3700 4800 3700
Wire Wire Line
	4800 3800 5000 3800
Wire Wire Line
	4400 3300 4400 3050
Wire Wire Line
	4400 3050 5400 3050
Wire Wire Line
	5400 3050 5400 3300
$Comp
L CONN_01X01 P3
U 1 1 5817A8A2
P 3850 3050
F 0 "P3" H 3850 3150 50  0000 C CNN
F 1 "CONN_01X01" V 3950 3050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 3850 3050 50  0001 C CNN
F 3 "" H 3850 3050 50  0000 C CNN
	1    3850 3050
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 P4
U 1 1 5817A8D1
P 3850 3550
F 0 "P4" H 3850 3650 50  0000 C CNN
F 1 "CONN_01X01" V 3950 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 3850 3550 50  0001 C CNN
F 3 "" H 3850 3550 50  0000 C CNN
	1    3850 3550
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 P5
U 1 1 5817A904
P 6000 3050
F 0 "P5" H 6000 3150 50  0000 C CNN
F 1 "CONN_01X01" V 6100 3050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 6000 3050 50  0001 C CNN
F 3 "" H 6000 3050 50  0000 C CNN
	1    6000 3050
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 P6
U 1 1 5817A931
P 6000 3550
F 0 "P6" H 6000 3650 50  0000 C CNN
F 1 "CONN_01X01" V 6100 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 6000 3550 50  0001 C CNN
F 3 "" H 6000 3550 50  0000 C CNN
	1    6000 3550
	1    0    0    -1  
$EndComp
NoConn ~ 5800 3050
NoConn ~ 5800 3550
NoConn ~ 3650 3550
NoConn ~ 3650 3050
$EndSCHEMATC
